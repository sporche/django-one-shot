from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoListTask


# Create your views here.
def todolist_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "list_of_lists": todo_list,
    }
    return render(request, "todos/todolist.html", context)


def list_details(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "list_items": details,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm()
        context = {
            "form": form,
        }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todolist)

    context = {
        "todolist_object": todolist,
        "form": form,
    }

    return render(request, "todos/edit.html", context)


def todo_listitem_update(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoListTask(request.POST, instance=todoitem)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListTask(instance=todoitem)

    context = {
        "todoitem_object": todoitem,
        "form": form,
    }

    return render(request, "todos/edit_tasks.html", context)


def delete_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list", id=id)
