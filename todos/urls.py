from django.urls import path
from todos.views import todolist_list, list_details, create_list
from todos.views import todo_list_update, todo_listitem_update, delete_list


urlpatterns = [
    path("", todolist_list, name="todo_list_list"),
    path("<int:id>", list_details, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("edit/<int:id>", todo_list_update, name="todo_list_update"),
    path(
        "edit/<int:id>/item", todo_listitem_update, name="todo_listitem_update"
    ),
    path("delete<int:id>", delete_list, name="delete_list"),
]
